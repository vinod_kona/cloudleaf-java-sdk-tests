package com.cloudleaf.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.cloudleaf.cloudos.sdk.ApiClient;
import com.cloudleaf.cloudos.sdk.api.AssetControllerApi;
import com.cloudleaf.cloudos.sdk.api.CustomerControllerApi;
import com.cloudleaf.cloudos.sdk.api.DataControllerApi;
import com.cloudleaf.cloudos.sdk.api.DeviceCommandControllerApi;
import com.cloudleaf.cloudos.sdk.api.DeviceStateControllerApi;
import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;
import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;
import com.cloudleaf.cloudos.sdk.api.UserControllerApi;

@Configuration
public class CloudLeafIntegrationConfig {
 
	private static String token="$2a$10$/fVQNKq7Pb0E3jPSRTpKuOYoyBAPwkWiOChQsvax99Fhu0/nPZCGq";
	//private static String token="$2a$10$McLTgHbWyuTReGLyDr7H2OiIqXEqKM2zH.V4EApHQtcCgV3neGili";
	
    @Bean
    public AssetControllerApi assetControllerApi() {
        return new AssetControllerApi(apiClient());
    }
    
    @Bean
    public CustomerControllerApi customerControllerApi() {
        return new CustomerControllerApi(apiClient());
    }
    
    @Bean
    public DataControllerApi dataControllerApi() {
        return new DataControllerApi(apiClient());
    }
    
    @Bean
    public DeviceCommandControllerApi deviceCommandControllerApi() {
        return new DeviceCommandControllerApi(apiClient());
    }
    
    @Bean
    public DeviceStateControllerApi deviceStateControllerApi() {
        return new DeviceStateControllerApi(apiClient());
    }
    
    @Bean
    public MetadataControllerApi metadataControllerApi() {
        return new MetadataControllerApi(apiClient());
    }
    
    @Bean
    public SensorTagControllerApi sensorTagControllerApi() {
        return new SensorTagControllerApi(apiClient());
    }
    
    @Bean
    public UserControllerApi userControllerApi() {
        return new UserControllerApi(apiClient());
    }
    
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
    
    @Bean
    public ApiClient apiClient() {
    	ApiClient apiClient = new ApiClient();
    	apiClient.addDefaultHeader("token",token);
        return apiClient;
    }
}