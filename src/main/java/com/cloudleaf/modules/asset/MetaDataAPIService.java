/*******************************************************************************
 * © Cloudleaf, Inc., all rights reserved. 
 * The contents of this file are the confidential and proprietary information of Cloudleaf, Inc.
 ******************************************************************************/
package com.cloudleaf.modules.asset;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.cloudleaf.cloudos.sdk.api.MetadataControllerApi;
import com.cloudleaf.cloudos.sdk.model.CharacteristicMeta;
import com.cloudleaf.cloudos.sdk.model.PeripheralMeta;
import com.cloudleaf.cloudos.sdk.model.ServiceMeta;
import com.cloudleaf.cloudos.utils.CloudosUtils;



@Service
public class MetaDataAPIService {

	@Autowired
	private MetadataControllerApi metadataControllerApi;
	protected static final Logger LOG = Logger.getLogger(MetaDataAPIService.class.getName()); 
	
	public PeripheralMeta createCharacteristicMeta(String pType,String sId,String cId) {
		LOG.info("creating object for characteristic meta");
		CharacteristicMeta cmeta=CloudosUtils.getCharacteristicMetaforUpdate();
		LOG.info("setting object for API");
		PeripheralMeta peripheralMeta=null;
		try{
			peripheralMeta=metadataControllerApi.addCharacteristicMetaUsingPOST(pType, sId, cId, cmeta);
			LOG.info("Created Characteristic meta with Id"+peripheralMeta.getIdentifier());
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return peripheralMeta;
	}
	
	public ServiceMeta getCharacteristicMeta(String pType,String sId,String cId) {
		LOG.info("get characteristic meta data");
		ServiceMeta serviceMeta=null;
		try{
			serviceMeta=metadataControllerApi.getCharacteristicMetadataUsingGET(pType, sId, cId);
			LOG.info("get service meta object"+serviceMeta);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return serviceMeta;
	}
	
	public String deleteCharacteristicMeta(String pType,String sId,String cId) {
		LOG.info("delete characteristic meta data");
		String status=null;
		try{
			status=metadataControllerApi.deleteCharacteristicMetadataUsingDELETE(pType, sId, cId);
			LOG.info("delete characteristic meta status"+status);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return status;
	}
	
	
		
}
