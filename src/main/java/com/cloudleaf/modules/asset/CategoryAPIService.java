/*******************************************************************************
 * © Cloudleaf, Inc., all rights reserved. 
 * The contents of this file are the confidential and proprietary information of Cloudleaf, Inc.
 ******************************************************************************/
package com.cloudleaf.modules.asset;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.cloudleaf.cloudos.sdk.api.CustomerControllerApi;
import com.cloudleaf.cloudos.sdk.model.Category;
import com.cloudleaf.cloudos.utils.CloudosUtils;



@Service
public class CategoryAPIService {

	@Autowired
	private CustomerControllerApi customerControllerApi;
	
	public CustomerControllerApi getCustomerControllerApi() {
		return customerControllerApi;
	}

	public void setCustomerControllerApi(CustomerControllerApi customerControllerApi) {
		this.customerControllerApi = customerControllerApi;
	}

	protected static final Logger LOG = Logger.getLogger(CategoryAPIService.class.getName()); 
	
	public Category createCategory(String id) {
		LOG.info("creating object for category");
		Category category=CloudosUtils.getCategotyforCreate(id);
		LOG.info("setting category object for API");
		Category createdCategory=null;
		try{
			createdCategory=customerControllerApi.addCategoryUsingPOST(category.getTenantId(), category);
			LOG.info("Created Category with status"+createdCategory);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return createdCategory;
	}
	
	public Category updateCategory(String id) {
		LOG.info("update object for category");
		Category category=CloudosUtils.getCategotyforUpdate(id);
		LOG.info("update category object for API");
		Category updatedCategory=null;
		try{
			updatedCategory=customerControllerApi.updateCategoryUsingPUT(id, category);
			LOG.info("Updated Category with status"+updatedCategory);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		return updatedCategory;
	}
	
	public List<Category> getCategoriesInTenant() {
		LOG.info("get categories in tenant");
		List<Category> categories=null;
		try{
			categories=customerControllerApi.getCategoriesUsingGET();
			LOG.info("get Categories with "+categories);
			for (Category category : categories) {
				LOG.info("get Category with id"+category.toString());
			}
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return categories;
	}
	
	public String deleteAssetCategory(String id) {
		LOG.info("delete object for category");
		
		String categoryStatus=null;
		try{
			categoryStatus=customerControllerApi.removeCategoryUsingDELETE(id);
			categoryStatus="OK";
			LOG.info("delete Category with status"+categoryStatus);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		return categoryStatus;
	}
	
	
}
