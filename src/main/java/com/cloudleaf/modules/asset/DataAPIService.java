/*******************************************************************************
 * © Cloudleaf, Inc., all rights reserved. 
 * The contents of this file are the confidential and proprietary information of Cloudleaf, Inc.
 ******************************************************************************/
package com.cloudleaf.modules.asset;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.cloudleaf.cloudos.sdk.api.DataControllerApi;
import com.cloudleaf.cloudos.sdk.api.DeviceStateControllerApi;
import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;
import com.cloudleaf.cloudos.sdk.model.Category;
import com.cloudleaf.cloudos.sdk.model.EventData;
import com.cloudleaf.cloudos.sdk.model.SensorTag;
import com.cloudleaf.cloudos.utils.CloudosUtils;



@Service
public class DataAPIService {

	@Autowired
	private DataControllerApi dataControllerApi;
	protected static final Logger LOG = Logger.getLogger(DataAPIService.class.getName()); 
	
	public String publishDeviceEvent(String id) {
		LOG.info("send publishDeviceEvent for category");
		EventData eventData=CloudosUtils.getSensorEventforUpdate(id);
		String status=null;
		try{
			status=dataControllerApi.postToStreamUsingPOST(id, eventData);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		return status;
	}
	
}
