/*******************************************************************************
 * © Cloudleaf, Inc., all rights reserved. 
 * The contents of this file are the confidential and proprietary information of Cloudleaf, Inc.
 ******************************************************************************/
package com.cloudleaf.modules.asset;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.cloudleaf.cloudos.sdk.api.AssetControllerApi;
import com.cloudleaf.cloudos.sdk.model.Asset;
import com.cloudleaf.cloudos.utils.CloudosUtils;


/* 
 * Description : This class is to create asset
 * 
 * 
 * 
 * API         :  POST /api/1/asset
 * 
 * Sample Request Body : 
 						{
							"category": "74d934c8-7838-46f0-9edc-5758d9dda47a",
							"externalId": "Asset1",
							"name": "Katerra Asset1"
						}
 * 
 * 
 */
@Service
public class AssetAPIService {

	@Autowired
	private AssetControllerApi assetControllerApi;
	
	protected static final Logger LOG = Logger.getLogger(AssetAPIService.class.getName()); 
	
	public Asset createAsset(String id) {
		LOG.info("creating object for asset");
		Asset asset=CloudosUtils.getAssetObjectforCreate(id);
		LOG.info("setting object for API");
		Asset createdAsset=null;
		try{
			createdAsset=assetControllerApi.createAssetUsingPOST(asset);
			LOG.info("Created Asset with Id"+createdAsset.getId());
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return createdAsset;
	}
	
	public String updateAsset(String id) {
		LOG.info("creating object for  update asset");
		Asset asset=CloudosUtils.getAssetObjectforUpdate();
		LOG.info("setting object for API");
		String updateAssetStatus=null;
		try{
			updateAssetStatus=assetControllerApi.updateAssetUsingPUT1(id, asset);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return updateAssetStatus;
	}
	
	public Asset getAsset(String id) {
		LOG.info("get object for asset");
		LOG.info("setting object for API");
		Asset asset=null;
		try{
			asset=assetControllerApi.getAssetUsingGET(id);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return asset;
	}
	
	public String deleteAsset(String id) {
		LOG.info("deleting asset");
		String status=null;
		try{
			assetControllerApi.deleteAssetUsingDELETE(id);
			status="OK";
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
			status="FAILURE";
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
			status="FAILURE";
		}
		
		return status;
	}
	
	public List<Asset> getAllAssetsOfType(String type) {
		LOG.info("get object for asset");
		List<Asset> assets=null;
		try{
			assets=assetControllerApi.findAllAssetsByTypeUsingGET(type);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return assets;
	}
	
	
}
