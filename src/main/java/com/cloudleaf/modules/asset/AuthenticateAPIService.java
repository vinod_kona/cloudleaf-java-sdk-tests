/*******************************************************************************
 * © Cloudleaf, Inc., all rights reserved. 
 * The contents of this file are the confidential and proprietary information of Cloudleaf, Inc.
 ******************************************************************************/
package com.cloudleaf.modules.asset;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;
import com.cloudleaf.cloudos.sdk.api.UserControllerApi;
import com.cloudleaf.cloudos.sdk.model.AccessToken;
import com.cloudleaf.cloudos.sdk.model.Asset;
import com.cloudleaf.cloudos.sdk.model.SensorTag;
import com.cloudleaf.cloudos.sdk.model.TaggedAsset;
import com.cloudleaf.cloudos.sdk.model.UserCredentials;
import com.cloudleaf.cloudos.utils.CloudosUtils;



@Service
public class AuthenticateAPIService {

	@Autowired
	private UserControllerApi userControllerApi;
	protected static final Logger LOG = Logger.getLogger(AuthenticateAPIService.class.getName()); 
	
	public AccessToken authenticateUser(String username,String password) {
		String status=null;
		UserCredentials userCredentials=new UserCredentials();
		userCredentials.setLogin(username);
		userCredentials.setSecret(password);
		AccessToken accessToken=null;
		try{
			accessToken=userControllerApi.loginUsingPOST(userCredentials);
			LOG.info("Login credentials"+status);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return accessToken;
	}
	
}
