/*******************************************************************************
 * © Cloudleaf, Inc., all rights reserved. 
 * The contents of this file are the confidential and proprietary information of Cloudleaf, Inc.
 ******************************************************************************/
package com.cloudleaf.modules.asset;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;
import com.cloudleaf.cloudos.sdk.model.Asset;
import com.cloudleaf.cloudos.sdk.model.SensorTag;
import com.cloudleaf.cloudos.sdk.model.TaggedAsset;
import com.cloudleaf.cloudos.utils.CloudosUtils;



@Service
public class SensorAPIService {

	@Autowired
	private SensorTagControllerApi sensorTagControllerApi;
	protected static final Logger LOG = Logger.getLogger(SensorAPIService.class.getName()); 
	
	public SensorTagControllerApi getSensorTagControllerApi() {
		return sensorTagControllerApi;
	}

	public void setSensorTagControllerApi(SensorTagControllerApi sensorTagControllerApi) {
		this.sensorTagControllerApi = sensorTagControllerApi;
	}

	public String createSensor(String id) {
		LOG.info("creating object for sensor");
		SensorTag sensorTag=CloudosUtils.getSensorTagforCreate(id);
		LOG.info("setting sensor object for API");
		String status=null;
		try{
			status=sensorTagControllerApi.createSensorTagUsingPOST(sensorTag);
			LOG.info("Created Sensor with status"+status);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return status;
	}
	
	public String updateSensor(String id) {
		LOG.info("update object for sensor");
		SensorTag sensorTag=CloudosUtils.getSensorTagforUpdate(id);
		LOG.info("setting sensor object for API");
		String status=null;
		try{
			status=sensorTagControllerApi.updateSensorTagUsingPUT(id, sensorTag);
			status="OK";
			LOG.info("Created Sensor with status"+status);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return status;
	}
	
	public SensorTag getSensor(String id) {
		LOG.info("get object for sensor");
		SensorTag sensorTag=null;
		try{
			sensorTag=sensorTagControllerApi.getSensorTagUsingGET(id,false);
			LOG.info("Get Sensor with Id"+sensorTag);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return sensorTag;
	}
	
	public String deleteSensor(String id) {
		LOG.info("delete sensor for Id"+id);
		String status=null;
		try{
			status=(String)sensorTagControllerApi.deleteSensorTagUsingDELETE(id);
			status="OK";
			LOG.info("deleted Sensor with status"+status);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return status;
	}
	
	public TaggedAsset provisionSensorToAsset(String sensorId,String assetId,String categoryId) {
		LOG.info("provision sensor for asset");
		TaggedAsset taggedAsset=new TaggedAsset();
		SensorTag sensorTag=CloudosUtils.getSensorTagToBind(sensorId);
		Asset asset=CloudosUtils.getAssetToBind(assetId, categoryId);
		
		taggedAsset.setSensorTag(sensorTag);
		taggedAsset.setAsset(asset);
		
		TaggedAsset provisionedTaggedAsset=null;
		try{
			provisionedTaggedAsset=sensorTagControllerApi.provisionTaggedAssetUsingPOST(taggedAsset);
			
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return provisionedTaggedAsset;
	}
	
	public String unbindSensorToAsset(String taggedAssetId) {
		LOG.info("provision sensor for asset");
		String status=null;
		try{
			status=sensorTagControllerApi.deProvisionTaggedAssetUsingDELETE(taggedAssetId);
			status="OK";
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return status;
	}
	
	public TaggedAsset getTaggedAssetDetails(String taggedAssetId) {
		
		TaggedAsset provisionedTaggedAsset=null;
		try{
			provisionedTaggedAsset=sensorTagControllerApi.getTaggedAssetUsingGET(taggedAssetId);
		}catch(RestClientException restClientException){
			LOG.info("RestClient Exception occured --"+restClientException.getMessage());
		}catch(Exception e){
			LOG.info("Exception occured --"+e.getMessage());
		}
		
		return provisionedTaggedAsset;
	}
	
	
}
