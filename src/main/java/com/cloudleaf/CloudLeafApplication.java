package com.cloudleaf;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import com.cloudleaf.cloudos.sdk.ApiClient;
import com.cloudleaf.cloudos.sdk.api.SensorTagControllerApi;
import com.cloudleaf.cloudos.sdk.model.AccessToken;
import com.cloudleaf.cloudos.sdk.model.Asset;
import com.cloudleaf.cloudos.sdk.model.Category;
import com.cloudleaf.cloudos.sdk.model.SensorTag;
import com.cloudleaf.cloudos.sdk.model.TaggedAsset;
import com.cloudleaf.cloudos.utils.CloudosUtils;
import com.cloudleaf.modules.asset.AssetAPIService;
import com.cloudleaf.modules.asset.AuthenticateAPIService;
import com.cloudleaf.modules.asset.CategoryAPIService;
import com.cloudleaf.modules.asset.DataAPIService;
import com.cloudleaf.modules.asset.SensorAPIService;

@SpringBootApplication
public class CloudLeafApplication implements CommandLineRunner {

	@Autowired
	private AssetAPIService assetAPIService;
	
	@Autowired
	private AuthenticateAPIService authenticateAPIService;
	
	@Autowired
	private SensorAPIService sensorAPIService;
	
	@Autowired
	private CategoryAPIService categoryAPIService;
	
	@Autowired
	private DataAPIService dataAPIService;
	
	@Value("${cloudleaf.api.userid}")
	private String userId;

	@Value("${cloudleaf.api.password}")
	private String password;
	
	private String sensorId;
	
	private String assetId;
	
	private String categoryId;
	
	private String taggedAssetId;

	private String id="58";
	
	protected static final Logger LOG = Logger.getLogger(CloudLeafApplication.class.getName());
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(CloudLeafApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		LOG.info("instantiating from Command Line Runner");
		AccessToken accessToken=authenticateAPIService.authenticateUser(userId, password);
		String token=accessToken.getToken();
		if(!StringUtils.isEmpty(token)){
			LOG.info("*******************creating sensor***************");
			sensorTagFunctionality();
			categoryFunctionality();
			assetFunctionality();
			bindAssetToSensor();
			sendSensorEvent();
			getAllAssetsOfCategory(categoryId);
			unbindAssetToSensor(taggedAssetId);
			deleteSensor(sensorId);
			deleteAsset(assetId);
			deleteAssetCategory(categoryId);
		}
	}
	

	private void unbindAssetToSensor(String taggedAssetId) {
		LOG.info("*******************unbindig asset to sensor***************");
		String status=sensorAPIService.unbindSensorToAsset(taggedAssetId);
		if(!StringUtils.isEmpty(status)){
			LOG.info("*******************asset unbinded successfully***************");
		}
	}

	private void sensorTagFunctionality() {
		SensorTag sensorTag=CloudosUtils.getSensorTagforCreate(id);
		String status=sensorAPIService.createSensor(id);
		//String status="OK";
		if(!StringUtils.isEmpty(status)){
			LOG.info("*******************sensor tag created successfully***************");
			LOG.info("*******************Fetching details of the sensor tag***************");
			//sensorId="E06089001B69";
			sensorId=sensorTag.getId();
			SensorTag tag=sensorAPIService.getSensor(sensorId);
			if(tag!=null){
				LOG.info("*******************Fetched sensor Tag successfuly***************"+tag.toString());
				LOG.info("*******************Updating sensor tag***************");
				
				status=sensorAPIService.updateSensor(tag.getId());
				if(!StringUtils.isEmpty(status)){
					LOG.info("******************* sensor Tag updated  successfuly***************");
				}else{
					LOG.info("*******************Error while updating sensor Tag***************"+status);
				}
			}else{
				LOG.info("*******************No sensor Tag Exists***************");
			}
		}
	}
	
	private void categoryFunctionality() {
		
		Category category=categoryAPIService.createCategory(id);
		categoryId=category.getId();
		if(!StringUtils.isEmpty(category)){
			LOG.info("*******************category created successfully***************");
			LOG.info("*******************Fetching details of the category***************");
			
			Category updatedCategory=categoryAPIService.updateCategory(category.getId());
			LOG.info("******************* category updated  successfuly***************"+updatedCategory.toString());
			
			List<Category> categories=categoryAPIService.getCategoriesInTenant();
			if(categories!=null){
				LOG.info("*******************Fetched categoris successfuly***************");
				LOG.info("*******************Updating category***************");
				
				if(!ObjectUtils.isEmpty(categories)){
					LOG.info("*******************Fetched categories***************");
				}else{
					LOG.info("*******************Error while updating category***************");
				}
			}else{
				LOG.info("*******************No Categories Exists***************");
			}
		}
	}
	
	private void assetFunctionality() {
		Asset asset=assetAPIService.createAsset(id);
		if(!StringUtils.isEmpty(asset)){
			LOG.info("*******************asset created successfully***************");
			LOG.info("*******************Fetching details of the asset***************");
			asset=assetAPIService.getAsset(asset.getId());
			if(!ObjectUtils.isEmpty(asset)){
				LOG.info("*******************Fetched asset successfuly***************");
				LOG.info("*******************Updating asset***************");
				assetId=asset.getId();
				String status=assetAPIService.updateAsset(asset.getId());
				LOG.info("******************* asset updated  with status***************"+status);
				
			}else{
				LOG.info("*******************No asset Exists***************");
			}
		}
	}
	
	private void bindAssetToSensor() {
		
		ApiClient apiClient = new ApiClient();
    	apiClient.addDefaultHeader("token","$2a$10$/fVQNKq7Pb0E3jPSRTpKuOYoyBAPwkWiOChQsvax99Fhu0/nPZCGq");
    	
    	SensorTagControllerApi sensorAPI=new SensorTagControllerApi(apiClient);
    	
		SensorAPIService sensorAPIService=new SensorAPIService();
		sensorAPIService.setSensorTagControllerApi(sensorAPI);
		
		TaggedAsset taggedAsset=sensorAPIService.provisionSensorToAsset(sensorId, assetId, categoryId);
		if(!ObjectUtils.isEmpty(taggedAsset)){
			LOG.info("*******************sensor provisioned to asset***************");
			taggedAssetId=taggedAsset.getId();
			taggedAsset=sensorAPIService.getTaggedAssetDetails(taggedAsset.getId());
			if(!ObjectUtils.isEmpty(taggedAsset)){
				LOG.info("*******************Fetched tagged asset successfuly***************");
			}else{
				LOG.info("*******************No asset Exists***************");
			}
		}
	}
	
	private void sendSensorEvent() {
		dataAPIService.publishDeviceEvent(assetId);
	}
	
	private void getAllAssetsOfCategory(String categoryId) {
		LOG.info("*******************get all assets of category***************");
		List<Asset> assets=assetAPIService.getAllAssetsOfType(categoryId);
		if(assets!=null){
			for (Asset asset : assets) {
				LOG.info("asset details --"+asset.toString());
			}
		}
	}
	
	private void deleteSensor(String sensorId) {
		// TODO Auto-generated method stub
		LOG.info("*******************delete sensor***************");
		String status=sensorAPIService.deleteSensor(sensorId);
		if(!StringUtils.isEmpty(status)){
			LOG.info("*******************sensor deleted successfuly***************");
		}else{
			LOG.info("*******************Error while deleting sensor***************");
		}
	}
	
	private void deleteAsset(String assetId) {
		// TODO Auto-generated method stub
		LOG.info("*******************delete asset***************");
		String status=assetAPIService.deleteAsset(assetId);
		if(!StringUtils.isEmpty(status)){
			LOG.info("*******************asset deleted successfuly***************");
		}else{
			LOG.info("*******************Error while deleting asset***************");
		}
	}
	
	private void deleteAssetCategory(String categoryId) {
		// TODO Auto-generated method stub
		LOG.info("*******************delete asset category***************");
		String status=categoryAPIService.deleteAssetCategory(categoryId);
		if(!StringUtils.isEmpty(status)){
			LOG.info("*******************category deleted successfuly***************");
		}else{
			LOG.info("*******************Error while deleting category***************");
		}
	}
}