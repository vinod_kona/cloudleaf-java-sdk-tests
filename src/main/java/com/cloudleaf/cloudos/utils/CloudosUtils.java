package com.cloudleaf.cloudos.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.cloudleaf.cloudos.sdk.ApiClient;
import com.cloudleaf.cloudos.sdk.model.Asset;
import com.cloudleaf.cloudos.sdk.model.Asset.StatusEnum;
import com.cloudleaf.cloudos.sdk.model.Category;
import com.cloudleaf.cloudos.sdk.model.CharacteristicMeta;
import com.cloudleaf.cloudos.sdk.model.EventData;
import com.cloudleaf.cloudos.sdk.model.IdCommand;
import com.cloudleaf.cloudos.sdk.model.PeripheralMeta;
import com.cloudleaf.cloudos.sdk.model.PeripheralTypeMeta;
import com.cloudleaf.cloudos.sdk.model.SensorTag;
import com.cloudleaf.cloudos.sdk.model.ServiceMeta;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;


public class CloudosUtils {
	private final static Logger LOGGER = Logger.getLogger(CloudosUtils.class.getName());
	private ClassLoader classLoader = getClass().getClassLoader();
	private static  String category="74d934c8-7838-46f0-9edc-5758d9dda47a";
	private static  String assetExternalId="Katerra Asset4";
	private static String token="$2a$10$0jwgdDXCmKUQ/jnMQ42uyO49XLi3/huouGkffqmmDfhM0YZVBaga.";
	private static String tenantId="21c01c14-426c-4a98-ae18-46e1cd89c1f9";
	
	/*public static Gson getGson(){
		Gson gson=JSON.createGson().registerTypeAdapter(Date.class,  new JsonDeserializer<Date>() { 
		   	   public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
				      return new Date(json.getAsJsonPrimitive().getAsLong()); 
				   }}).create();
		return gson;
	}*/
	
	public static void setApiParameters(ApiClient apiClient) {
		apiClient.addDefaultHeader("token",token);
	}
	
	
	
	public static long getCurrentTime(){
		return Calendar.getInstance().getTime().getTime();
	}
	
	static final long ONE_MINUTE_IN_MILLIS=60000;//millisecs
	public static long getNextDatMilliSeconds(){
		
		Date dt=new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(dt); 
		c.add(Calendar.DATE, 1);
		return c.getTimeInMillis();
	}
	
	public static void configureMessageConverters(RestTemplate restTemplate) {
		List<HttpMessageConverter<?>> converters=new ArrayList<>();
	    final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
	    final ObjectMapper objectMapper = new ObjectMapper();
	    objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
	    objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
	    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
	    converter.setObjectMapper(objectMapper);
	    converters.add(converter);
	    restTemplate.setMessageConverters(converters);
	}
	
	public static  Asset getAssetObjectforCreate(String id){
		
		Asset asset=new Asset();
		asset.setCategory(category);
		asset.setExternalId("Asset4"+id);
		asset.setName(assetExternalId);
		
		return asset;
	}
	
	public static  Asset getAssetObjectforUpdate(){
		
		Asset asset=new Asset();
		asset.setId("850488e2-af39-4ee9-a700-9e4dd412862c");
		asset.setExternalId(assetExternalId);
		asset.setName("Katerra Asset One");
		asset.setTenantId("21c01c14-426c-4a98-ae18-46e1cd89c1f9");
		asset.setStatus(StatusEnum.UNMONITORED);
		asset.setSortOrder(0);
		asset.setCreatedAt(new Date());
		asset.setModifiedAt(new Date());
		asset.setCategory(category);
		asset.setCategoryName("Katerra Assets");
		return asset;
	}
	
	public static SensorTag getSensorTagforCreate(String id){
		
		SensorTag sensorTag=new SensorTag();
		sensorTag.setId("E123456789"+id);
		sensorTag.setType("PV_ADAPTER");
		//sensorTag.setCreatedAt(new Date());
		//sensorTag.setModifiedAt(new Date());
		/*sensorTag.setStatus(com.cloudleaf.cloudos.sdk.model.SensorTag.StatusEnum.READY);
		sensorTag.setTenantId(tenantId);
		Map<String,String> properties=new HashMap<>();
		properties.put("mfgdate","1531912824");
		sensorTag.setProperties(properties);*/
		return sensorTag;
	}
	
	public static SensorTag getSensorTagforUpdate(String id){
		
		SensorTag sensorTag=new SensorTag();
		sensorTag.setId(id);
		sensorTag.setType("PV_ADAPTER");
		sensorTag.setStatus(com.cloudleaf.cloudos.sdk.model.SensorTag.StatusEnum.READY);
		sensorTag.setCreatedAt(new Date());
		sensorTag.setModifiedAt(new Date());
		sensorTag.setTenantId("21c01c14-426c-4a98-ae18-46e1cd89c1f9");
		Map<String,String> properties=new HashMap<>();
		properties.put("mfgdate","1554714392");
		properties.put("manufacturer","cloudleaf");
		sensorTag.setProperties(properties);
		return sensorTag;
	}
	
	public static SensorTag getSensorTagToBind(String id){
		SensorTag sensorTag=new SensorTag();
		sensorTag.setId(id);
		sensorTag.setType("PV_ADAPTER");
		sensorTag.setStatus(com.cloudleaf.cloudos.sdk.model.SensorTag.StatusEnum.READY);
		sensorTag.setCreatedAt(new Date());
		sensorTag.setModifiedAt(new Date());
		sensorTag.setTenantId(tenantId);
		
		Map<String,String> properties=new HashMap<>();
		properties.put("mfgdate","1531912824");
		
		sensorTag.setProperties(properties);
		return sensorTag;
	}
	
	public static Asset getAssetToBind(String id,String categoryId){
		
		Asset asset=new Asset();
		asset.setId(id);
		asset.setExternalId(assetExternalId);
		asset.setTenantId(tenantId);
		asset.setStatus(StatusEnum.UNMONITORED);
		asset.setSortOrder(0);
		asset.setCreatedAt(new Date());
		asset.setModifiedAt(new Date());
		asset.setCategory(categoryId);
		asset.setCategoryName("KA15");
		
		Map<String,String> properties=new HashMap<>();
		properties.put("mfgdate","1531912824");
		
		return asset;
	}


	public static CharacteristicMeta getCharacteristicMetaforUpdate(){
		
		CharacteristicMeta characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("MaxLoadCurrent");
		characteristicMeta.setName("Max load current");
		characteristicMeta.setType("com.clouldeaf.characteristic.lm.loadcurrent");
		characteristicMeta.setConverter("FloatType");
		characteristicMeta.setUnitType("Amps");
		
		Map<String,String> parameters=new HashMap<>();
		parameters.put("category", "Data");
		parameters.put("dimension", "TimeSeries");
		parameters.put("datatype", "Number");
		parameters.put("variablity", "Dynamic");
		parameters.put("source", "Computed");
		parameters.put("audited", "No");
		parameters.put("resetFreq", "NA");
		parameters.put("resetValue", "NA");
		
		characteristicMeta.setConfigParams(parameters);
		return characteristicMeta;
	}
	
	public static PeripheralTypeMeta getPheripheralTypeforCreate(){
		
		PeripheralTypeMeta peripheralTypeMeta=new PeripheralTypeMeta();
		peripheralTypeMeta.setRegEx("^PV_ADAPTER$");
		
		IdCommand idCommand=new IdCommand();
		idCommand.setCId("ManufacturerData");
		idCommand.setInAdvert(true);
		
		peripheralTypeMeta.setDeviceClass("Adapter_Device");
		return peripheralTypeMeta;
	}
	
	public static PeripheralMeta getPheripheralTypeMetaforUpdate(){
		
		PeripheralMeta peripheralMeta=new PeripheralMeta();
		peripheralMeta.setIdentifier("PV_ADAPTER");
		peripheralMeta.setType("Adapter_Device");
		
		List<ServiceMeta> serviceMetaLists=new ArrayList<>();
		
		ServiceMeta serviceMeta=new ServiceMeta();
		
		CharacteristicMeta characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("UnderVoltageWarning");
		characteristicMeta.setName("PMO Under Voltage Warning");
		characteristicMeta.setType("com.clouldeaf.characteristic.pmo.undervoltagewarning");
		characteristicMeta.setUnitType("Binary");
		
		serviceMeta.addCharacteristicsItem(characteristicMeta);
		
		characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("Voltage");
		characteristicMeta.setName("PMO Voltage");
		characteristicMeta.setType("com.clouldeaf.characteristic.pmo.voltage");
		characteristicMeta.setUnitType("Volts");
		
		serviceMeta.addCharacteristicsItem(characteristicMeta);
		
		characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("OverVoltageWarning");
		characteristicMeta.setName("PMO Over Voltage Warning");
		characteristicMeta.setType("com.clouldeaf.characteristic.pmo.overvoltagewarning");
		characteristicMeta.setUnitType("Binary");
		
		serviceMeta.addCharacteristicsItem(characteristicMeta);
		
		characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("OverCurrentWarning");
		characteristicMeta.setName("PMO Over Current Warning");
		characteristicMeta.setType("com.clouldeaf.characteristic.pmo.overcurrentwarning");
		characteristicMeta.setUnitType("Binary");
		
		serviceMeta.addCharacteristicsItem(characteristicMeta);
		
		characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("Current");
		characteristicMeta.setName("PMO Current");
		characteristicMeta.setType("com.clouldeaf.characteristic.pmo.current");
		characteristicMeta.setUnitType("Amps");
		
		serviceMeta.addCharacteristicsItem(characteristicMeta);
		
		characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("Current");
		characteristicMeta.setName("PMO Current");
		characteristicMeta.setType("com.clouldeaf.characteristic.pmo.current");
		characteristicMeta.setUnitType("Amps");
		
		serviceMeta.addCharacteristicsItem(characteristicMeta);
		
		characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("Power");
		characteristicMeta.setName("PMO Current");
		characteristicMeta.setType("com.clouldeaf.characteristic.pmo.power");
		characteristicMeta.setUnitType("Watts");
		
		serviceMeta.addCharacteristicsItem(characteristicMeta);
		serviceMeta.setIdentifier("PMO");
		serviceMeta.setName("Power Monitor Output Service");
		serviceMeta.setType("com.cloudleaf.adapter.pmo");
		
		serviceMetaLists.add(serviceMeta);
		
		serviceMeta=new ServiceMeta();
		serviceMeta.setIdentifier("PMI");
		serviceMeta.setName("Power Monitor Input Service");
		serviceMeta.setType("com.cloudleaf.adapter.pmi");
		
		characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("UnderVoltageWarning");
		characteristicMeta.setName("PMI Under Voltage Warning");
		characteristicMeta.setType("com.clouldeaf.characteristic.pmi.undervoltagewarning");
		characteristicMeta.setUnitType("Binary");
		
		serviceMeta.addCharacteristicsItem(characteristicMeta);
		
		characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("Voltage");
		characteristicMeta.setName("PMI Voltage");
		characteristicMeta.setType("com.clouldeaf.characteristic.pmi.voltage");
		characteristicMeta.setUnitType("Volts");
		
		serviceMeta.addCharacteristicsItem(characteristicMeta);
		
		characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("OverVoltageWarning");
		characteristicMeta.setName("PMI Over Voltage Warning");
		characteristicMeta.setType("com.clouldeaf.characteristic.pmi.overvoltagewarning");
		characteristicMeta.setUnitType("Binary");
		
		serviceMeta.addCharacteristicsItem(characteristicMeta);
		
		
		characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("OverCurrentWarning");
		characteristicMeta.setName("PMI Over Current Warning");
		characteristicMeta.setType("com.clouldeaf.characteristic.pmi.overcurrentwarning");
		characteristicMeta.setUnitType("Binary");
		
		serviceMeta.addCharacteristicsItem(characteristicMeta);
		
		
		characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("Current");
		characteristicMeta.setName("PMI Current");
		characteristicMeta.setType("com.clouldeaf.characteristic.pmi.current");
		characteristicMeta.setUnitType("Amps");
		
		serviceMeta.addCharacteristicsItem(characteristicMeta);
		
		
		characteristicMeta=new CharacteristicMeta();
		characteristicMeta.setIdentifier("Power");
		characteristicMeta.setName("PMI Power");
		characteristicMeta.setType("com.clouldeaf.characteristic.pmi.power");
		characteristicMeta.setUnitType("Watts");
		
		serviceMeta.addCharacteristicsItem(characteristicMeta);
		
		serviceMetaLists.add(serviceMeta);
		
		peripheralMeta.services(serviceMetaLists);
		return peripheralMeta;
	}
	
	public static Category getCategotyforCreate(String id){
		Category category=new Category();
		category.setName("KA1"+id);
		category.setDescription("katerra assets 15");
		category.setStatus(com.cloudleaf.cloudos.sdk.model.Category.StatusEnum.ACTIVE);
		category.setTenantId(tenantId);
		return category;
	}
	
	public static Category getCategotyforUpdate(String id){
		Category category=new Category();
		category.setName("KA15");
		category.setTenantId(tenantId);
		category.setDescription("katerra assets 15");
		category.setStatus(com.cloudleaf.cloudos.sdk.model.Category.StatusEnum.ACTIVE);
		category.setCreatedAt(new Date());
		category.setModifiedAt(new Date());
		category.setId(id);
		
		List<String> metrics=new ArrayList<>();
		metrics.add("Asset State Change");
		metrics.add("Condition Thresholds");
		metrics.add("Dwelltime Thresholds");
		metrics.add("Inventory Thresholds");
		metrics.add("Presence & Movement");
		
		category.setMetrics(metrics);
		return category;
	}

	public static EventData getSensorEventforUpdate(String id){
		
		EventData eventData=new EventData();
		eventData.setLeafId("fe30653c-467f-401a-9646-67b10378e1c9");
		eventData.setPheripheralId("769a0224-2c07-4f31-9b01-8c18199a1c5b");
		eventData.setsId("PMI");
		eventData.setcId("Current");
		eventData.setTime(new Date());
		eventData.setEvtTime(new Date());
		eventData.setId(id);
		eventData.setValue("20");
		return eventData;
	}

}
